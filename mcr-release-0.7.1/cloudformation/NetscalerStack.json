{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "CloudFormation Template to provision the Netscaler Instance",
	"Parameters": {
		"KeyName": {
			"Type": "AWS::EC2::KeyPair::KeyName",
			"Description": "Name of an existing EC2 KeyPair to enable SSH access to the bastion host",
			"MinLength": "1",
			"MaxLength": "64",
			"AllowedPattern": "[-_ a-zA-Z0-9]*",
			"ConstraintDescription": "can contain only alphanumeric characters, spaces, dashes and underscores."
		},
		"ProvisionBastion": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision Bastion or not"
		},
		"NumberOfAZ": {
			"Type": "Number",
			"MinValue": "1",
			"MaxValue": "2",
			"Default": "2",
			"Description": "Number of Availabilty Zones"
		},
		"NumberOfSecondaryIPs": {
			"Type": "Number",
			"MinValue": "0",
			"MaxValue": "17",
			"Default": "8",
			"Description": "Number of Secondary IPs to be crearted"
		},
		"BOISystemHealthMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIEC2Monitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		},
		"Environment": {
			"Type": "String",
			"Description": "Environment to be provisioned",
			"Default": "dev",
			"AllowedValues": ["dev", "test", "sit", "uat", "preprod", "prod"]
		},
		"ClientName": {
			"Type": "String",
			"Description": "Client name"
		},
		"NetScalerVIPSubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Netscaler VIP Host Subnet for Availabilty Zone #1"
		},
		"NetScalerVIPSubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Netscaler VIP Host Subnet for Availabilty Zone #2"
		},
		"NetScalerNSIPSubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Netscaler MGMT Host Subnet for Availabilty Zone #1"
		},
		"NetScalerNSIPSubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Netscaler VIP Host Subnet for Availabilty Zone #2"
		},
		"LBSubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Loadbalancer Subnet for Availabilty Zone #1"
		},
		"LBSubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Loadbalancer Host Subnet for Availabilty Zone #2"
		},
		"NetscalerMGMTSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup::Id",
			"Description": "Security Group for Netscaler Management Host"
		},
		"BastionSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup::Id",
			"Description": "Security Group for Bastion Host"
		},
		"NetscalerVIPSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup::Id",
			"Description": "Security Group for Netscaler VIP Host"
		},
		"LBSecurityGroup": {
			"Type": "AWS::EC2::SecurityGroup::Id",
			"Description": "Security Group for Loadbalancer Host"
		},
		"NetScalerNSIPNACL": {
			"Type": "String",
			"Description": "Network ACL for Netscaler NSIP Host"
		},
		"NetScalerVIPNACL": {
			"Type": "String",
			"Description": "Network ACL for Netscaler VIP Host"
		},
		"BastionSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ1",
			"Default": "10.1.20.128/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"BastionSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ2",
			"Default": "10.1.20.144/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"VPCCidrBlock": {
			"Type": "String",
			"Description": "Cidr block of the VPC",
			"Default": "10.1.16.0/20",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		}
	},
	"Mappings": {
		"RegionToAmiMapForNetscalerAmi": {
			"us-east-1": {
				"NetscalerAmi": "ami-8ea3d8f4"
			},
			"us-west-2": {
				"NetscalerAmi": "ami-19919679"
			},
			"eu-west-1": {
				"NetscalerAmi": "ami-6727a51e"
			},
			"eu-central-1": {
				"NetscalerAmi": "ami-6093070f"
			}
		}
	},
	"Conditions": {
		"SecondAZUsed": {
			"Fn::Equals": [{
				"Ref": "NumberOfAZ"
			}, "2"]
		},
		"ProvisionBastionActivated": {
			"Fn::Equals": [{
				"Ref": "ProvisionBastion"
			}, "yes"]
		}
	},
	"Resources": {
		"NetscalerInstanceRole": {
			"Type": "AWS::IAM::Role",
			"Properties": {
				"AssumeRolePolicyDocument": {
					"Version": "2012-10-17",
					"Statement": [{
						"Effect": "Allow",
						"Principal": {
							"Service": [
								"ec2.amazonaws.com"
							]
						},
						"Action": ["sts:AssumeRole"]
					}]
				},
				"Path": "/"
			}
		},
		"InstanceProfileForNetscalerInstanceRole": {
			"Type": "AWS::IAM::InstanceProfile",
			"Properties": {
				"Roles": [{
					"Ref": "NetscalerInstanceRole"
				}]
			}
		},
		"NetscalerHostAZ1": {
			"Type": "AWS::EC2::Instance",
			"Metadata": {
				"Comment": "Netscaler Instance"
			},
			"Properties": {
				"InstanceType": "m4.xlarge",
				"BlockDeviceMappings": [{
					"DeviceName": "/dev/xvda",
					"Ebs": {
						"VolumeSize": "50",
						"VolumeType": "gp2"
					}
				}],
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForNetscalerInstanceRole"
				},
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForNetscalerAmi", {
						"Ref": "AWS::Region"
					}, "NetscalerAmi"]
				},
				"NetworkInterfaces": [{
					"GroupSet": [{
						"Ref": "NetscalerMGMTSecurityGroup"
					}],
					"DeviceIndex": "0",
					"AssociatePublicIpAddress": "true",
					"SecondaryPrivateIpAddressCount": "1",
					"DeleteOnTermination": "true",
					"SubnetId": {
						"Ref": "NetScalerNSIPSubnetAZ1"
					}
				}],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["CitrixNetscaler_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					}
				}]
			}
		},
		"ClientENIAZ1": {
			"Type": "AWS::EC2::NetworkInterface",
			"Properties": {
				"GroupSet": [{
					"Ref": "NetscalerVIPSecurityGroup"
				}],
				"Description": "ENI connected to Entrypoint subnet",
				"SecondaryPrivateIpAddressCount": {
					"Ref": "NumberOfSecondaryIPs"
				},
				"SubnetId": {
					"Ref": "NetScalerVIPSubnetAZ1"
				}
			}
		},
		"ClientENIAttachmentAZ1": {
			"Type": "AWS::EC2::NetworkInterfaceAttachment",
			"DependsOn": "NetscalerHostAZ1",
			"Properties": {
				"InstanceId": {
					"Ref": "NetscalerHostAZ1"
				},
				"NetworkInterfaceId": {
					"Ref": "ClientENIAZ1"
				},
				"DeviceIndex": "1"
			}
		},
		"ServerENIAZ1": {
			"Type": "AWS::EC2::NetworkInterface",
			"Properties": {
				"GroupSet": [{
					"Ref": "LBSecurityGroup"
				}],
				"Description": "ENI connected to ELB subnet",
				"SubnetId": {
					"Ref": "LBSubnetAZ1"
				}
			}
		},
		"ServerENIAttachmentAZ1": {
			"Type": "AWS::EC2::NetworkInterfaceAttachment",
			"DependsOn": "NetscalerHostAZ1",
			"Properties": {
				"InstanceId": {
					"Ref": "NetscalerHostAZ1"
				},
				"NetworkInterfaceId": {
					"Ref": "ServerENIAZ1"
				},
				"DeviceIndex": "2"
			}
		},
		"NetscalerHostAZ2": {
			"Type": "AWS::EC2::Instance",
			"Metadata": {
				"Comment": "Netscaler Instance"
			},
			"Properties": {
				"InstanceType": "m4.xlarge",
				"BlockDeviceMappings": [{
					"DeviceName": "/dev/xvda",
					"Ebs": {
						"VolumeSize": "50",
						"VolumeType": "gp2"
					}
				}],
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForNetscalerInstanceRole"
				},
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForNetscalerAmi", {
						"Ref": "AWS::Region"
					}, "NetscalerAmi"]
				},
				"NetworkInterfaces": [{
					"GroupSet": [{
						"Ref": "NetscalerMGMTSecurityGroup"
					}],
					"DeviceIndex": "0",
					"AssociatePublicIpAddress": "true",
					"SecondaryPrivateIpAddressCount": "1",
					"DeleteOnTermination": "true",
					"SubnetId": {
						"Ref": "NetScalerNSIPSubnetAZ2"
					}
				}],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["CitrixNetscaler_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					}
				}]
			}
		},
		"ClientENIAZ2": {
			"Type": "AWS::EC2::NetworkInterface",
			"Properties": {
				"GroupSet": [{
					"Ref": "NetscalerVIPSecurityGroup"
				}],
				"Description": "ENI connected to Entrypoint subnet",
				"SecondaryPrivateIpAddressCount": {
					"Ref": "NumberOfSecondaryIPs"
				},
				"SubnetId": {
					"Ref": "NetScalerVIPSubnetAZ2"
				}
			}
		},
		"ClientENIAttachmentAZ2": {
			"Type": "AWS::EC2::NetworkInterfaceAttachment",
			"DependsOn": "NetscalerHostAZ2",
			"Properties": {
				"InstanceId": {
					"Ref": "NetscalerHostAZ2"
				},
				"NetworkInterfaceId": {
					"Ref": "ClientENIAZ2"
				},
				"DeviceIndex": "1"
			}
		},
		"ServerENIAZ2": {
			"Type": "AWS::EC2::NetworkInterface",
			"Properties": {
				"GroupSet": [{
					"Ref": "LBSecurityGroup"
				}],
				"Description": "ENI connected to ELB subnet",
				"SubnetId": {
					"Ref": "LBSubnetAZ2"
				}
			}
		},
		"ServerENIAttachmentAZ2": {
			"Type": "AWS::EC2::NetworkInterfaceAttachment",
			"DependsOn": "NetscalerHostAZ2",
			"Properties": {
				"InstanceId": {
					"Ref": "NetscalerHostAZ2"
				},
				"NetworkInterfaceId": {
					"Ref": "ServerENIAZ2"
				},
				"DeviceIndex": "2"
			}
		},
		"SSHBastionToNetscalerMGMTIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "22",
				"GroupId": {
					"Ref": "NetscalerMGMTSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"ToPort": "22"
			}
		},
		"HTTPSBastionToNetscalerMGMTIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "443",
				"GroupId": {
					"Ref": "NetscalerMGMTSecurityGroup"
				},
				"IpProtocol": "tcp",
				"CidrIp": "0.0.0.0/0",
				"ToPort": "443"
			}
		},
		"HTTPSBastionToNetscalerVIpIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "443",
				"GroupId": {
					"Ref": "NetscalerVIPSecurityGroup"
				},
				"IpProtocol": "tcp",
				"CidrIp": "0.0.0.0/0",
				"ToPort": "443"
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ1ToNetscalerMGMTSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ1 To NetscalerMGMT Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerNSIPNACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ2ToNetscalerMGMTSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ2 To NetscalerMGMT Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerNSIPNACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"EphimeralAllowInboundNACLEntryForBastionSubnet": {
				"Type": "AWS::EC2::NetworkAclEntry",
				"Metadata": {
					"Comment": "Network ACL Entry inbound Ephimeral traffic For NetscalerMGMT Subnet"
				},
				"Properties": {
					"NetworkAclId": {
						"Ref": "NetScalerNSIPNACL"
					},
					"RuleNumber": "120",
					"Protocol": "6",
					"RuleAction": "allow",
					"Egress": "false",
					"CidrBlock":  "0.0.0.0/0",
					"PortRange": {
						"From": "1024",
						"To": "65535"
					}
				}
			},
		"HTTPSAllowInboundNACLEntryToNetscalerMGMTSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound HTTPS traffic For NetscalerMGMT Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerNSIPNACL"
				},
				"RuleNumber": "140",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": "0.0.0.0/0",
				"PortRange": {
					"From": "443",
					"To": "443"
				}
			}
		},
		"EphimeralAllowOutboundNACLEntryFromMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound traffic From NetscalerMGMT Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerNSIPNACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": "0.0.0.0/0",
				"PortRange": {
					"From": "1024",
					"To": "65535"
				}
			}
		},
		"HTTPSAllowOutboundNACLEntryToNetscalerMGMTSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound HTTPS traffic For NetscalerMGMT Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerNSIPNACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": "0.0.0.0/0",
				"PortRange": {
					"From": "443",
					"To": "443"
				}
			}
		},
		"EphimeralAllowInboundNACLEntryForNetscalerVIPSubnet": {
				"Type": "AWS::EC2::NetworkAclEntry",
				"Metadata": {
					"Comment": "Network ACL Entry inbound Ephimeral traffic For NetscalerVIP Subnet"
				},
				"Properties": {
					"NetworkAclId": {
						"Ref": "NetScalerVIPNACL"
					},
					"RuleNumber": "100",
					"Protocol": "6",
					"RuleAction": "allow",
					"Egress": "false",
					"CidrBlock":  "0.0.0.0/0",
					"PortRange": {
						"From": "1024",
						"To": "65535"
					}
				}
			},
		"HTTPAllowInboundNACLEntryFromBastionAZ2ToNetscalerVIPSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound HTTPS traffic For NetscalerVIP Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerVIPNACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": "0.0.0.0/0",
				"PortRange": {
					"From": "443",
					"To": "443"
				}
			}
		},
		"EphimeralAllowOutboundNACLEntryFromNetscalerVIPSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound traffic From NetscalerVIP Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "NetScalerVIPNACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": "0.0.0.0/0",
				"PortRange": {
					"From": "1024",
					"To": "65535"
				}
			}
		},
		"AlarmForInstanceStatusCheckOfNetscalerHostAZ1": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for Bastion servers it goes active when the status check of the Netscaler AZ1 host fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for Netscaler server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "InstanceId",
					"Value": {
						"Ref": "NetscalerHostAZ1"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1"
			}
		},
		"CPUHighAlarmForNetscalerHostAZ1": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for Bastion servers it goes active when the CPU Utilization for Netscaler AZ1 host is more than 80 %"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOIEC2Monitors"
				}],
				"AlarmDescription": "Instance status check alarm for Netscaler server autoscaling group",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "InstanceId",
					"Value": {
						"Ref": "NetscalerHostAZ1"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"AlarmForInstanceStatusCheckOfNetscalerHostAZ2": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for Bastion servers it goes active when the status check of the Netscaler AZ2 host fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for Netscaler server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "InstanceId",
					"Value": {
						"Ref": "NetscalerHostAZ2"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1"
			}
		},
		"CPUHighAlarmForNetscalerHostAZ2": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for Bastion servers it goes active when the CPU Utilization for Netscaler AZ2 host is more than 80 %"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOIEC2Monitors"
				}],
				"AlarmDescription": "Instance status check alarm for Netscaler server autoscaling group",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "InstanceId",
					"Value": {
						"Ref": "NetscalerHostAZ2"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		}
	}
}
