#!/usr/bin/python
import boto3
import json
import argparse
from botocore.exceptions import ClientError
class ContainerAutoScaling:
    """Ecs Service Auto Scaling"""
    def __init__(self,resourceType,clusterName,serviceName,availabilityZone=None,aws_access_key_id=None,aws_secret_access_key=None):
        if not availabilityZone and aws_access_key_id and aws_secret_access_key:
            self.client = boto3.client(resourceType)
        elif availabilityZone and not aws_access_key_id and aws_secret_access_key:
            self.client = boto3.client(resourceType,availabilityZone)
        else:
            self.client = boto3.client(resourceType,availabilityZone, aws_access_key_id=aws_access_key_id,aws_secret_access_key=aws_secret_access_key)
            self.availabilityZone = availabilityZone
        self.clusterName = clusterName
        self.serviceName = serviceName
    def __ruturnNone__(self,responseStatus):
        if responseStatus is None:
            return True
        else:
            return False
    def registerScalableTarget(self,minCapacity,ecsAutoscaleRole):
        response = self.client.register_scalable_target(
            MaxCapacity=10,
            MinCapacity=minCapacity,
            ResourceId="service/{0}/{1}".format(self.clusterName,self.serviceName),
            RoleARN=ecsAutoscaleRole,
            ScalableDimension='ecs:service:DesiredCount',
            ServiceNamespace='ecs',
        )
        return response
    def putScalablePolicy(self,targetValue,ScaleOutCoolDownTime,ScaleInCoolDownTime):
         response = self.client.put_scaling_policy(
         PolicyName = "{0}-{1}-Policy".format(self.clusterName,self.serviceName),
         ServiceNamespace = "ecs",
         ResourceId = "service/{0}/{1}".format(self.clusterName,self.serviceName),
         ScalableDimension = "ecs:service:DesiredCount",
         PolicyType = "TargetTrackingScaling",
         TargetTrackingScalingPolicyConfiguration = {
            "TargetValue" : targetValue,
            "PredefinedMetricSpecification": {
                "PredefinedMetricType": "ECSServiceAverageCPUUtilization"
            },
            'ScaleOutCooldown': ScaleOutCoolDownTime,
            'ScaleInCooldown': ScaleInCoolDownTime,
         }
         )
         return response
def main():
    parser = argparse.ArgumentParser(description="Container AutoScaling")
    parser.add_argument('--region', type=str, help="reqion required", required=True)
    parser.add_argument('--aws_access_key_id', type=str, help="aws_access_key_id Required", required=False)
    parser.add_argument('--aws_secret_access_key', type=str, help="aws_secret_access_key Required", required=False)
    parser.add_argument('--cluster_name',type=str,help="cluter name of ecs service")
    parser.add_argument('--service_name',type=str,help="service name of ecs service")
    parser.add_argument('--target_value',type=float, help="The target value for the metric")
    parser.add_argument('--min_Capacity',type=int, default=1, help="the minimum value task definition when scale in event aquire")
    parser.add_argument('--scale_out_cooldown_time',type=int, default=60, help="The amount of time, in seconds, after a scale out activity completes before another scale out activity can start")
    parser.add_argument('--scale_in_cooldown_time',type=int, default=60, help="The amount of time, in seconds, after a scale in activity completes before another scale in activity can start")
    parser.add_argument('--ecs_auto_scale_role',type=str,help="The ARN of an IAM role that allows Application Auto Scaling to modify the scalable target on your behalf")
    aws = parser.parse_args()
    if aws.aws_access_key_id and aws.aws_secret_access_key is None:
        containerScalingObject=ContainerAutoScaling(resourceType='application-autoscaling',clusterName=aws.cluster_name,serviceName=aws.service_name,availabilityZone=aws.region)
    else:
        containerScalingObject=ContainerAutoScaling(resourceType='application-autoscaling',clusterName=aws.cluster_name,serviceName=aws.service_name,availabilityZone=aws.region)
    containerScalingObject.registerScalableTarget(minCapacity=aws.min_Capacity,ecsAutoscaleRole=aws.ecs_auto_scale_role)
    response = containerScalingObject.putScalablePolicy(targetValue=aws.target_value,ScaleOutCoolDownTime=aws.scale_out_cooldown_time,ScaleInCoolDownTime=aws.scale_in_cooldown_time)
    print response

if __name__ == '__main__':
    main()
