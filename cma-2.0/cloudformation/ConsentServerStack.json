{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "CloudFormation Template to provision the Consent Server Instances",
	"Parameters": {
		"KeyName": {
			"Type": "AWS::EC2::KeyPair::KeyName",
			"Description": "Name of an existing EC2 KeyPair to enable SSH access to the bastion host",
			"MinLength": "1",
			"MaxLength": "64",
			"AllowedPattern": "[-_ a-zA-Z0-9]*",
			"ConstraintDescription": "can contain only alphanumeric characters, spaces, dashes and underscores."
		},
		"ProvisionNetScaler": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision NetScaler or not"
		},
		"NumberOfAZ": {
			"Type": "Number",
			"MinValue": "1",
			"MaxValue": "2",
			"Default": "2",
			"Description": "Number of Availabilty Zones"
		},
		"BOIScaleUpMonitors": {
			"Type": "String",
			"Description": "SNS Topic for Scale Up"
		},
		"BOIScaleDownMonitors": {
			"Type": "String",
			"Description": "SNS Topic for Scale Down"
		},
		"BOISystemHealthMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIEC2Monitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		},
		"Environment": {
			"Type": "String",
			"Description": "Environment to be provisioned",
			"Default": "dev",
			"AllowedValues": ["dev", "test", "sit", "uat", "preprod", "prod"]
		},
		"ClientName": {
			"Type": "String",
			"Description": "Client name"
		},
		"UISubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "UI Host Subnet for Availabilty Zone #1"
		},
		"UISubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "UI Host Subnet for Availabilty Zone #2"
		},
		"EncryptedTextS3Bucket": {
			"Type": "String",
			"Default": "psd2-encrypted-data-sandbox-release-boi-prod",
			"Description": "Encrypted S3 bucket"
		},
		"HttpProxyIporDNS": {
			"Description": "IP address or DNS name of the http proxy",
			"Type": "String"
		},
		"BastionSecurityGroup": {
			"Description": "SecurityGroup for Bastion host",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"ConsentSecurityGroup": {
			"Description": "Security Groups for Microservices servers",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"ZuulSecurityGroup": {
			"Description": "Security Groups for Zuul servers",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"UINACL": {
			"Type": "String",
			"Description": "Network ACL for UI Host"
		},
		"BastionSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ1",
			"Default": "10.1.20.128/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"BastionSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ2",
			"Default": "10.1.20.144/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"ZuulSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of ZuulSubnetAZ1",
			"Default": "10.1.25.0/26",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"ZuulSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of ZuulSubnetAZ2",
			"Default": "10.1.25.64/26",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"VPCCidrBlock": {
			"Type": "String",
			"Description": "Cidr block of the VPC",
			"Default": "10.1.16.0/20",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		}
	},
	"Mappings": {
		"RegionToAmiMapForECSAMI": {
			"us-east-1": {
				"ECSAmi": "ami-65bc7318"
			},
			"us-west-2": {
				"ECSAmi": "ami-b90b86c1"
			},
			"eu-west-1": {
				"ECSAmi": "ami-18058860"
			},
			"eu-central-1": {
				"ECSAmi": "ami-b968bad6"
			}
		}
	},
	"Conditions": {
		"SecondAZUsed": {
			"Fn::Equals": [{
				"Ref": "NumberOfAZ"
			}, "2"]
		},
		"ProvisionNetScalerActivated": {
			"Fn::Equals": [{
				"Ref": "ProvisionNetScaler"
			}, "yes"]
		}
	},
	"Resources": {
		"ConsentServerCluster": {
			"Type": "AWS::ECS::Cluster",
			"Properties": {
				"ClusterName": "ConsentServerCluster"
			}
		},
		"ConsentApplicationRepo": {
			"Type": "AWS::ECR::Repository",
			"Properties": {
				"RepositoryName": "consent-application"
			}
		},
		"LogsGroupForConsentServer": {
			"Type": "AWS::Logs::LogGroup",
			"Properties": {
				"LogGroupName": {
					"Fn::Join": ["", ["/var/log/ConsentServer_", {
							"Ref": "ClientName"
						},
						{
							"Ref": "Environment"
						}
					]]
				}
			}
		},
		"ConsentInstanceRole": {
			"Type": "AWS::IAM::Role",
			"Properties": {
				"AssumeRolePolicyDocument": {
					"Version": "2012-10-17",
					"Statement": [{
						"Effect": "Allow",
						"Principal": {
							"Service": [
								"ec2.amazonaws.com",
								"ssm.amazonaws.com"
							]
						},
						"Action": ["sts:AssumeRole"]
					}]
				},
				"Path": "/",
				"Policies": [{
						"PolicyName": "cloudwatch-log-agent-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"logs:CreateLogGroup",
										"logs:CreateLogStream",
										"logs:PutLogEvents",
										"logs:DescribeLogStreams"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:/var/log/ConsentServer_",
												{
													"Ref": "ClientName"
												},
												{
													"Ref": "Environment"
												}, ":*"
											]]
										},
										{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:AWS_ECS", ":*"
											]]
										}
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ec2:DescribeTags"
									],
									"Resource": [
										"*"
									]
								}
							]
						}
					},
					{
						"PolicyName": "ssm-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ec2messages:GetMessages",
										"ec2messages:AcknowledgeMessage",
										"ec2messages:DeleteMessage",
										"ec2messages:FailMessage",
										"ec2messages:GetEndpoint",
										"ec2messages:SendReply",
										"ssm:DescribeInstanceProperties",
										"ssm:SendCommand",
										"ssm:GetDocument",
										"ssm:DescribeDocumentParameters"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ssm:UpdateInstanceInformation",
										"ssm:ListInstanceAssociations"
									],
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ec2", {
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "instance/*"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": "ssm:ListAssociations",
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ssm",
											{
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "*"
										]]
									}]
								}
							]
						}
					},
					{
						"PolicyName": "kms-decrypt",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"kms:Decrypt"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": "s3:ListAllMyBuckets",
									"Resource": "arn:aws:s3:::*"
								},
								{
									"Effect": "Allow",
									"Action": [
										"s3:GetObject",
										"s3:ListBucket"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}, "/*"]]
										}
									]
								}
							]
						}
					},
					{
						"PolicyName": "ecs-access",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ecr:GetDownloadUrlForLayer",
										"ecr:BatchGetImage",
										"ecr:DescribeImages",
										"ecr:DescribeRepositories",
										"ecr:ListImages",
										"ecr:BatchCheckLayerAvailability",
										"ecr:GetRepositoryPolicy"
									],
									"Resource": [{
										"Fn::Join": ["", ["arn:aws:ecr:", {
												"Ref": "AWS::Region"
											}, ":",
											{
												"Ref": "AWS::AccountId"
											}, ":", "repository", "/consent-application"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ecs:DeregisterContainerInstance",
										"ecs:DiscoverPollEndpoint",
										"ecs:Poll",
										"ecs:RegisterContainerInstance",
										"ecs:StartTelemetrySession",
										"ecs:Submit*",
										"ecr:GetAuthorizationToken"
									],
									"Resource": "*"
								}
							]
						}
					}
				]
			}
		},
		"InstanceProfileForConsentInstanceRole": {
			"Type": "AWS::IAM::InstanceProfile",
			"Properties": {
				"Roles": [{
					"Ref": "ConsentInstanceRole"
				}]
			}
		},
		"LaunchConfigForConsentServerCluster": {
			"Type": "AWS::AutoScaling::LaunchConfiguration",
			"Metadata": {
				"Comment": "LaunchConfiguration template for Consent Application Instances"
			},
			"Properties": {
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForECSAMI", {
						"Ref": "AWS::Region"
					}, "ECSAmi"]
				},
				"InstanceType": "m4.xlarge",
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForConsentInstanceRole"
				},
				"UserData": {
					"Fn::Base64": {
						"Fn::Join": ["\n", [
							"#!/bin/bash",
							"sed -i '/^export http_proxy/d' /etc/bashrc", "\n",
							"sed -i '/^export https_proxy/d' /etc/bashrc", "\n",
							"source /etc/bashrc",
							"touch /tmp/log.txt", "\n",
							{
								"Fn::Join": [
									"", [
										"echo HttpProxyIporDNS=",
										{
											"Ref": "HttpProxyIporDNS"
										},
										" >> /etc/bashrc \n"
									]
								]
							},
							"echo export http_proxy='http://${HttpProxyIporDNS}' >> /etc/bashrc",
							"echo export https_proxy='http://${HttpProxyIporDNS}' >> /etc/bashrc",
							"echo export NO_PROXY=169.254.169.254,.webservices.com,.mockfoundationservices.com >> /etc/bashrc",
							"source /etc/bashrc",
							"echo \"proxy=http://$HttpProxyIporDNS\" >> /etc/yum.conf",
							"echo \"$$: $(date +%s.%N | cut -b1-13)\" > /var/lib/cloud/instance/sem/config_yum_http_proxy",
							"echo \"export HTTP_PROXY=http://$HttpProxyIporDNS/\" >> /etc/sysconfig/docker",
							"echo \"export NO_PROXY=169.254.169.254\" >> /etc/sysconfig/docker",
							"echo \"$$: $(date +%s.%N | cut -b1-13)\" > /var/lib/cloud/instance/sem/config_docker_http_proxy",
							"echo \"HTTP_PROXY=$HttpProxyIporDNS\" >> /etc/ecs/ecs.config",
							"echo \"NO_PROXY=169.254.169.254,169.254.170.2,/var/run/docker.sock\" >> /etc/ecs/ecs.config",
							"echo \"$$: $(date +%s.%N | cut -b1-13)\" > /var/lib/cloud/instance/sem/config_ecs-agent_http_proxy",
							"echo \"Status is YES\" ",
							"service docker restart",
							"echo $http_proxy > /tmp/log.txt",
							"echo $https_proxy >> /tmp/log.txt",
							"echo ECS_CLUSTER=ConsentServerCluster >> /etc/ecs/ecs.config", "\n",
							"if [ -f /var/log/agentlog.log ]; then",
							"rm -f /var/log/agent.log",
							"fi",
							"if [ -f /var/log/conf_file/configuration* ]; then",
							"rm -f /var/log/conf_file/configuration*",
							"fi",
							"sed -i '/^export PROMPT_COMMAND/d' /etc/bashrc",
							"sed -i '/^session_log_command/d' /etc/bashrc",
							"sed -i '/has logged in/d' /etc/bashrc",
							"sed -i '/has logged out/d' /etc/bashrc",
							"echo 'export PROMPT_COMMAND=$(logger -p local6.notice \"User $USER has logged in\")' >> /etc/bashrc",
							"echo -e 'session_log_command() { local status=$?;local command;command=$(history -a >(tee -a $HISTFILE));if [[ -n \"$command\" ]]; then logger -p local6.notice \"Logged-in usr $USER [$$]: Executing command: $command\"; history -c; history -r;fi }' >> /etc/bashrc",
							"echo -e 'export PROMPT_COMMAND=session_log_command' >> /etc/bashrc",
							"echo alias exit='export PROMPT_COMMAND=$(logger -p local6.notice \"User $USER has logged out\") && exit' >> /etc/bashrc",
							"echo alias logout='export PROMPT_COMMAND=$(logger -p local6.notice \"User $USER has logged out\") && exit' >> /etc/bashrc",
							"sed -i \"s/^alias exit=/alias exit='/\" /etc/bashrc",
							"sed -i \"s/^alias logout=/alias logout='/\" /etc/bashrc",
							"sed -i \"s/exit$/exit'/\" /etc/bashrc",
							"source /etc/bashrc",
							"touch /tmp/proxyvars",
							"echo 'http_proxy = $http_proxy' >> /tmp/proxyvars",
							"echo 'https_proxy = $https_proxy' >> /tmp/proxyvars",
							"echo 'no_proxy=$NO_PROXY' >> /tmp/proxyvars",
							"if [ -s /etc/issue ]; then",
							"sed -i \"1s/.*/Amazon Linux AMI release 2017.03/\" /etc/issue",
							"else",
							"sed -i '/^Amazon/d' /etc/issue",
							"echo -e 'Amazon Linux AMI release 2017.03' >> /etc/issue",
							"fi",
							"touch /etc/rsyslog.d/bash.conf",
							"touch /var/log/commands.log",
							"touch /var/log/agentlog.txt",
							"sed -i '/commands.log/d' /etc/rsyslog.d/bash.conf",
							"sed -i '/& ~/d' /etc/rsyslog.d/bash.conf",
							"echo -e 'local6.* /var/log/commands.log' > /etc/rsyslog.d/bash.conf",
							"echo -e '& ~' >> /etc/rsyslog.d/bash.conf",
							"service rsyslog restart",
							"sed -i '/commands.log/d' /etc/logrotate.d/syslog",
							"sed -i '1 i\/var/log/commands.log' /etc/logrotate.d/syslog",
							"mkdir -p /var/log/conf_file",
							"service rsyslog restart",
							"wget -O /root/ec2-metadata http://s3.amazonaws.com/ec2metadata/ec2-metadata",
							"chmod +x /root/ec2-metadata",
							"touch /var/log/commands.log /var/log/image_id.txt /var/log/test1 /var/log/test2",
							"mkdir -p /home/ec2-user/aws_scripts",
							"cd /home/ec2-user/aws_scripts",
							"cd /root",
							"cp /home/ec2-user/aws_scripts/awscli-bundle.zip /root/",
							"unzip -o awscli-bundle.zip",
							"ln -s /usr/local/aws/bin/pip /usr/bin/pip",
							"./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws",
							"echo $(/root/ec2-metadata -i | awk -F\" \" '{print $2}') > /var/log/image_id.txt",
							"if [ -f /tmp/var_value.txt ]; then",
							"rm -f /tmp/var_value.txt",
							"touch /tmp/var_value.txt",
							"fi",
							"cp /home/ec2-user/aws_scripts/awslogs-agent-setup.py /var/log/conf_file/",
							"INSTANCE_ID=$(/root/ec2-metadata --instance-id | cut -d' ' -f2)",
							"echo $INSTANCE_ID >> /tmp/var_value.txt",
							"REGION=`/root/ec2-metadata -z | grep -Po \"(us|sa|eu|ap)-(north|south|central)?(east|west)?-[0-9]+\"`",
							"echo $REGION >> /tmp/var_value.txt",
							"TagName=`aws ec2 describe-tags --filters Name=resource-id,Values=${INSTANCE_ID} Name=key,Values=Name --query Tags[].Value --output text --region ${REGION}`",
							"echo $TagName >> /tmp/var_value.txt",
							"echo -e \"[general]\nstate_file = /var/awslogs/state/agent-state\n[/var/log/messages]\nfile = /var/log/commands.log\nlog_group_name = /var/log/${TagName}\nlog_stream_name = $(hostname)\ndatetime_format = %b %d %H:%M:%S\" > /var/log/conf_file/configuration_$(/root/ec2-metadata -i | awk -F\" \" '{print $2}')",
							"sleep 5",
							"mkdir -p /var/awslogs/etc",
							"if [ -f /var/awslogs/etc/proxy.conf ]; then",
							"rm -f /var/awslogs/etc/proxy.conf",
							"fi",
							"echo -e \"HTTP_PROXY=${HttpProxyIporDNS}\nHTTPS_PROXY=${HttpProxyIporDNS}\nNO_PROXY=169.254.169.254\" > /var/awslogs/etc/proxy.conf",
							"service awslogs restart",
							"echo \"Restarted service awslogs\"",
							"cd /var/log/conf_file",
							"chmod +x ./awslogs-agent-setup.py",
							"cp -f /home/ec2-user/aws_scripts/awslogs.tar /var/",
							"cp -f /home/ec2-user/aws_scripts/aws /var/awslogs/bin/",
							"cp -f /home/ec2-user/aws_scripts/activate /var/awslogs/bin/",
							"ls -ltr  /var/awslogs/bin/",
							"ls -ltr /var/awslogs/bin/aws /var/awslogs/bin/activate",
							"chmod +x /var/awslogs/bin/aws",
							"cd /var",
							"cp -f /home/ec2-user/aws_scripts/awslogs.tar /var/",
							"/bin/tar -xvf awslogs.tar",
							"echo -e 'starting awslogs-agent-setup.py script run'",
							"cd /var/log/conf_file",
							"/usr/bin/python awslogs-agent-setup.py -n -r $REGION -c /var/log/conf_file/configuration_$(/root/ec2-metadata -i | cut -d' ' -f2) | tee /var/log/agentlog.txt 2>&1",
							"sleep 2",
							"if [ -f /var/awslogs/etc/proxy.conf ]; then",
							"rm -f /var/awslogs/etc/proxy.conf",
							"fi",
							"echo -e \"HTTP_PROXY=${HttpProxyIporDNS}\nHTTPS_PROXY=${HttpProxyIporDNS}\nNO_PROXY=169.254.169.254\" > /var/awslogs/etc/proxy.conf",
							"service awslogs restart",
							"if [ -f /var/awslogs/etc/proxy.conf ]; then",
							"rm -f /var/awslogs/etc/proxy.conf",
							"fi",
							"echo -e \"HTTP_PROXY=${HttpProxyIporDNS}\nHTTPS_PROXY=${HttpProxyIporDNS}\nNO_PROXY=169.254.169.254\" > /var/awslogs/etc/proxy.conf",
							"service awslogs restart",
							"yum install -y /home/ec2-user/aws_scripts/amazon-ssm-agent.rpm",
							"if [ -f /etc/init/amazon-ssm-agent.conf ];then",
							"sed -i '22ienv http_proxy=http://'\"${HttpProxyIporDNS}\"'' /etc/init/amazon-ssm-agent.conf",
							"sed -i '23ienv https_proxy=http://'\"${HttpProxyIporDNS}\"'' /etc/init/amazon-ssm-agent.conf",
							"sed -i '24ienv HTTP_PROXY=http://'\"${HttpProxyIporDNS}\"'' /etc/init/amazon-ssm-agent.conf",
							"sed -i '25ienv HTTPS_PROXY=http://'\"${HttpProxyIporDNS}\"'' /etc/init/amazon-ssm-agent.conf",
							"sed -i '26ienv no_proxy=169.254.169.254,.webservices.com' /etc/init/amazon-ssm-agent.conf",
							"fi",
							"stop amazon-ssm-agent",
							"start amazon-ssm-agent",
							"sudo service chronyd start",
							"sudo yum remove -y rpcbind",
							"sudo yum remove -y nfs-utils"
						]]
					}
				},
				"SecurityGroups": [{
					"Ref": "ConsentSecurityGroup"
				}]
			}
		},
		"AutoScalingGroupForConsentServerCluster": {
			"Type": "AWS::AutoScaling::AutoScalingGroup",
			"DependsOn": "ConsentServerCluster",
			"Metadata": {
				"Comment": "Auto scaling group for Consent Server Instances"
			},
			"Properties": {
				"DesiredCapacity": "2",
				"HealthCheckType": "EC2",
				"LaunchConfigurationName": {
					"Ref": "LaunchConfigForConsentServerCluster"
				},

				"MaxSize": "2",
				"MetricsCollection": [{
					"Granularity": "1Minute",
					"Metrics": [
						"GroupDesiredCapacity"
					]
				}],
				"MinSize": "2",
				"NotificationConfigurations": [{
						"TopicARN": {
							"Ref": "BOIScaleUpMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_LAUNCH",
							"autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
						]
					},
					{
						"TopicARN": {
							"Ref": "BOIScaleDownMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_TERMINATE",
							"autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
						]
					}
				],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["ConsentServer_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					},
					"PropagateAtLaunch": "true"
				}],
				"VPCZoneIdentifier": [{
						"Ref": "UISubnetAZ1"
					},
					{
						"Fn::If": [
							"SecondAZUsed",
							{
								"Ref": "UISubnetAZ1"
							},
							{
								"Ref": "AWS::NoValue"
							}
						]
					}
				]
			},
			"UpdatePolicy": {
				"AutoScalingScheduledAction": {
					"IgnoreUnmodifiedGroupSizeProperties": "true"
				},
				"AutoScalingRollingUpdate": {
					"MaxBatchSize": "1",
					"MinInstancesInService": "1",
					"PauseTime": "PT10M",
					"WaitOnResourceSignals": "false"
				}
			}
		},
		"SSHBastionToConsentIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "22",
				"GroupId": {
					"Ref": "ConsentSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"ToPort": "22"
			}
		},
		"ZuulToConsentIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "0",
				"GroupId": {
					"Ref": "ConsentSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "ZuulSecurityGroup"
				},
				"ToPort": "65535"
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ1ToMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ1 To Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ2ToMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ2 To Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"EphimeralAllowInboundNACLEntryFromMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound traffic From Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "120",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "32768",
					"To": "65535"
				}
			}
		},
		"VPCAllAllowInboundNACLEntryFromMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound traffic From VPC Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "130",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "0",
					"To": "65535"
				}
			}
		},
		"EphimeralAllowOutboundNACLEntryFromMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound Ephimeral traffic From Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "32768",
					"To": "65535"
				}
			}
		},
		"VPCAllAllowOutboundNACLEntryFromMicroservicesSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Consent Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "UINACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "0",
					"To": "65535"
				}
			}
		},
		"ScaleUpPolicyForConsentServerCluster": {
			"Type": "AWS::AutoScaling::ScalingPolicy",
			"Metadata": {
				"Comment": "ScaleUp policy attached to the Auto scaling group for ConsentServerCluster server"
			},
			"Properties": {
				"AdjustmentType": "ChangeInCapacity",
				"AutoScalingGroupName": {
					"Ref": "AutoScalingGroupForConsentServerCluster"
				},
				"Cooldown": "60",
				"ScalingAdjustment": "1"
			}
		},
		"ScaleDownPolicyForConsentServerCluster": {
			"Type": "AWS::AutoScaling::ScalingPolicy",
			"Metadata": {
				"Comment": "ScaleDown policy attached to the Auto scaling group for ConsentServerCluster"
			},
			"Properties": {
				"AdjustmentType": "ChangeInCapacity",
				"AutoScalingGroupName": {
					"Ref": "AutoScalingGroupForConsentServerCluster"
				},
				"Cooldown": "60",
				"ScalingAdjustment": "-1"
			}
		},
		"AlarmForInstanceStatusCheckOfConsentServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConsentServerCluster servers it goes active when the status check of any instance in ConsentServerClusterautoscaling group fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for ConsentServerCluster server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "AutoScalingGroupName",
					"Value": {
						"Ref": "AutoScalingGroupForConsentServerCluster"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1",
				"Unit": "Count"
			}
		},
		"CPUHighAlarmForConsentServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConsentServerCluster servers it goes active when the CPU utilization is more than 75 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "ScaleUpPolicyForConsentServerCluster"
					},
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU alarm for my ConsentServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConsentServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"CPULowAlarmForConsentServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConsentServerCluster servers it goes active when the CPU Utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "ScaleDownPolicyForConsentServerCluster"
					},
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU Low alarm for my ConsentServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConsentServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		},
		"MemoryHighAlarmForConsentServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConsentServerCluster servers it goes active when the Memory utilization is more than 75 percent for 5 consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "ScaleUpPolicyForConsentServerCluster"
					},
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory High alarm for my ConsentServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConsentServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"MemoryLowAlarmForConsentServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConsentServerCluster servers it goes active when the Memory utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "ScaleDownPolicyForConsentServerCluster"
					},
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory Low alarm for my ConsentServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions":[{
					"Name": "ClusterName",
					"Value": "ConsentServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		}
	}
}
