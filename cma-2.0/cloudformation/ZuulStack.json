{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "CloudFormation Template to provision the Zuul Instance",
	"Parameters": {
		"KeyName": {
			"Type": "AWS::EC2::KeyPair::KeyName",
			"Description": "Name of an existing EC2 KeyPair to enable SSH access to the bastion host",
			"MinLength": "1",
			"MaxLength": "64",
			"AllowedPattern": "[-_ a-zA-Z0-9]*",
			"ConstraintDescription": "can contain only alphanumeric characters, spaces, dashes and underscores."
		},
		"ProvisionNetScaler": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision NetScaler or not"
		},
		"ProvisionProxy": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision Proxy or not"
		},
		"NumberOfAZ": {
			"Type": "Number",
			"MinValue": "1",
			"MaxValue": "2",
			"Default": "2",
			"Description": "Number of Availabilty Zones"
		},
		"PrivateHostedZoneId": {
			"Type": "AWS::Route53::HostedZone::Id",
			"Description": "Private Hosted Zone Id for internal domains"
		},
		"PrivateDomainName": {
			"Description": "Domain Name of the project",
			"Type": "String",
			"Default": "webservices.com"
		},
		"ZuulSubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Zuul Host Subnet for Availabilty Zone #1"
		},
		"ZuulSubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Zuul Host Subnet for Availabilty Zone #2"
		},
		"Environment": {
			"Type": "String",
			"Description": "Environment to be provisioned",
			"Default": "dev",
			"AllowedValues": ["dev", "test", "sit", "uat", "preprod", "prod"]
		},
		"ClientName": {
			"Type": "String",
			"Description": "Client name"
		},
		"EncryptedTextS3Bucket": {
			"Type": "String",
			"Default": "psd2-encrypted-data-sandbox-release-boi-prod",
			"Description": "Encrypted S3 bucket"
		},
		"CloudFormationS3Bucket": {
			"Type": "String",
			"Default": "",
			"Description": "Encrypted S3 bucket"
		},
		"HttpProxyIporDNS": {
			"Description": "IP address or DNS name of the http proxy",
			"Type": "String"
		},
		"BastionSecurityGroup": {
			"Description": "SecurityGroup for bastion host",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"LBSecurityGroup": {
			"Description": "SecurityGroup for LB SecurityGroup",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"EdgeServerSecurityGroup": {
			"Description": "Security Groups for Mule Runtime servers",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"ZUULACL": {
			"Type": "String",
			"Description": "Network ACL for UI Host"
		},
		"BastionSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ1",
			"Default": "10.1.20.128/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"BastionSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of BastionSubnetAZ2",
			"Default": "10.1.20.144/28",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"LBSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of LBSubnetAZ1",
			"Default": "10.1.18.0/24",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"LBSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of LBSubnetAZ2",
			"Default": "10.1.19.0/24",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"VPCCidrBlock": {
			"Type": "String",
			"Description": "Cidr block of the VPC",
			"Default": "10.1.16.0/20",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"EdgeServerTargetGroup": {
			"Type": "String",
			"Description": "EdgeServerTargetGroup ARN"
		},
		"BOISystemHealthMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIEC2Monitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		},
		"BOIScaleUpMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIScaleDownMonitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		}
	},
	"Mappings": {
		"RegionToAmiMapForECSAMI": {
			"us-east-1": {
				"ECSAmi": "ami-65bc7318"
			},
			"us-west-2": {
				"ECSAmi": "ami-18058860"
			},
			"eu-west-1": {
				"ECSAmi": "ami-18058860"
			},
			"eu-central-1": {
				"ECSAmi": "ami-b968bad6"
			}
		}
	},
	"Conditions": {
		"SecondAZUsed": {
			"Fn::Equals": [{
				"Ref": "NumberOfAZ"
			}, "2"]
		},
		"ProvisionNetScalerActivated": {
			"Fn::Equals": [{
				"Ref": "ProvisionNetScaler"
			}, "yes"]
		}
	},
	"Resources": {
		"EdgeServerCluster": {
			"Type": "AWS::ECS::Cluster",
			"Properties": {
				"ClusterName": "EdgeServerCluster"
			}
		},
		"EdgeServerRepo": {
			"Type": "AWS::ECR::Repository",
			"Properties": {
				"RepositoryName": "edge-server"
			}
		},
		"LogsGroupForEdgeServer": {
			"Type": "AWS::Logs::LogGroup",
			"Properties": {
				"LogGroupName": {
					"Fn::Join": ["", ["/var/log/EdgeServer_", {
							"Ref": "ClientName"
						},
						{
							"Ref": "Environment"
						}
					]]
				}
			}
		},
		"ZuulInstanceRole": {
			"Type": "AWS::IAM::Role",
			"Properties": {
				"AssumeRolePolicyDocument": {
					"Version": "2012-10-17",
					"Statement": [{
						"Effect": "Allow",
						"Principal": {
							"Service": [
								"ec2.amazonaws.com",
								"ssm.amazonaws.com"
							]
						},
						"Action": ["sts:AssumeRole"]
					}]
				},
				"Path": "/",
				"Policies": [{
						"PolicyName": "cloudwatch-log-agent-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"logs:CreateLogGroup",
										"logs:CreateLogStream",
										"logs:PutLogEvents",
										"logs:DescribeLogStreams"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:/var/log/EdgeServer_",
												{
													"Ref": "ClientName"
												},
												{
													"Ref": "Environment"
												}, ":*"
											]]
										},
										{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:AWS_ECS", ":*"
											]]
										}
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ec2:DescribeTags"
									],
									"Resource": [
										"*"
									]
								}
							]
						}
					},
					{
						"PolicyName": "ssm-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ec2messages:GetMessages",
										"ec2messages:AcknowledgeMessage",
										"ec2messages:DeleteMessage",
										"ec2messages:FailMessage",
										"ec2messages:GetEndpoint",
										"ec2messages:SendReply",
										"ssm:DescribeInstanceProperties",
										"ssm:SendCommand",
										"ssm:GetDocument",
										"ssm:DescribeDocumentParameters"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ssm:UpdateInstanceInformation",
										"ssm:ListInstanceAssociations"
									],
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ec2", {
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "instance/*"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": "ssm:ListAssociations",
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ssm",
											{
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "*"
										]]
									}]
								}
							]
						}
					},
					{
						"PolicyName": "kms-decrypt",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"kms:Decrypt"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": "s3:ListAllMyBuckets",
									"Resource": "arn:aws:s3:::*"
								},
								{
									"Effect": "Allow",
									"Action": [
										"s3:GetObject",
										"s3:ListBucket"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}, "/*"]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "CloudFormationS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "CloudFormationS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}, "/*"]]
										}
									]
								}
							]
						}
					},
					{
						"PolicyName": "ecs-access",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ecr:GetDownloadUrlForLayer",
										"ecr:BatchGetImage",
										"ecr:DescribeImages",
										"ecr:DescribeRepositories",
										"ecr:ListImages",
										"ecr:BatchCheckLayerAvailability",
										"ecr:GetRepositoryPolicy"
									],
									"Resource": [{
										"Fn::Join": ["", ["arn:aws:ecr:", {
												"Ref": "AWS::Region"
											}, ":",
											{
												"Ref": "AWS::AccountId"
											}, ":", "repository", "/edge-server"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ecs:DeregisterContainerInstance",
										"ecs:DiscoverPollEndpoint",
										"ecs:Poll",
										"ecs:RegisterContainerInstance",
										"ecs:StartTelemetrySession",
										"ecs:Submit*",
										"ecr:GetAuthorizationToken"
									],
									"Resource": "*"
								}
							]
						}
					},
					{
						"PolicyName": "route53-domain-update-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
								"Effect": "Allow",
								"Action": [
									"route53:ChangeResourceRecordSets"
								],
								"Resource": [{
									"Fn::Join": ["/", ["arn:aws:route53:::hostedzone", {
										"Ref": "PrivateHostedZoneId"
									}]]
								}]
							}]
						}
					}
				]
			}
		},
		"InstanceProfileForZuulInstanceRole": {
			"Type": "AWS::IAM::InstanceProfile",
			"Properties": {
				"Roles": [{
					"Ref": "ZuulInstanceRole"
				}]
			}
		},
		"LaunchConfigForEdgeServer": {
			"Type": "AWS::AutoScaling::LaunchConfiguration",
			"Metadata": {
				"Comment": "LaunchConfiguration template for Edge server"
			},
			"Properties": {
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForECSAMI", {
						"Ref": "AWS::Region"
					}, "ECSAmi"]
				},
				"InstanceType": "m4.xlarge",
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForZuulInstanceRole"
				},
				"UserData": {
					"Fn::Base64": {
						"Fn::Join": ["\n", [
							"#!/bin/bash",
							{
								"Fn::Join": [
									"", [
										"HostedZoneId=",
										{
											"Ref": "PrivateHostedZoneId"
										},
										"\n",
										"S3_BUCKET_NAME=",
										{
											"Ref": "ClientName"
										}, "-", {
											"Ref": "CloudFormationS3Bucket"
										}, "-", {
											"Ref": "Environment"
										},
										"\n",
										"HttpProxyIporDNS=",
										{
											"Ref": "HttpProxyIporDNS"
										},
										"\n",
										"PrivateDomainName=",
										{
											"Ref": "PrivateDomainName"
										},
										"\n",
										"REGION=",
										{
											"Ref": "AWS::Region"
										},
										"\n"
									]
								]
							},
							"export https_proxy=${HttpProxyIporDNS}",
							"if [[ -n ${S3_BUCKET_NAME} && -n ${HttpProxyIporDNS} &&  -n ${PrivateDomainName} && -n ${REGION} ]]; then",
							"aws s3 --no-verify-ssl cp s3://${S3_BUCKET_NAME}/user-data/common.sh /root/common.sh --region ${REGION}",
							"else",
							"echo \"please provide Variables Value for S3_BUCKET_NAME, HttpProxyIporDNS,PrivateDomainName,REGION\"",
							"fi",
							"if [[ -f \"/root/common.sh\" ]]; then",
							"source \"/root/common.sh\"",
							"else",
							"echo \"common libs does not exist\"",
							"exit 1",
							"fi",
							"ECS_CLUSTER_NAME=\"EdgeServerCluster\"",
							"if (( $?==0 )); then",
							"ProxySettings ${HttpProxyIporDNS} \"/etc/bashrc /etc/sysconfig/docker\"",
							"ProxySettingsForYum ${HttpProxyIporDNS}",
							"InstallPackage /home/ec2-user/aws_scripts/amazon-ssm-agent.rpm",
							"ProxySettingsforSSM ${HttpProxyIporDNS}",
							"StartOrStopService amazon-ssm-agent",
							"if [[ -n ${ECS_CLUSTER_NAME} ]]; then",
							"ReplaceOrAppendOnFile \"ECS_CLUSTER=\" \"${ECS_CLUSTER_NAME}\" \"/etc/ecs/ecs.config\"",
							"ProxySettingsForEcsAgent ${HttpProxyIporDNS}",
							"restartDocker ecs",
							"fi",
							"ConfigureAwsLogAgent ${HttpProxyIporDNS}",
							"fi",
							"sudo service chronyd start",
							"sudo yum remove -y rpcbind",
							"sudo yum remove -y nfs-utils"
						]]
					}
				},
				"SecurityGroups": [{
					"Ref": "EdgeServerSecurityGroup"
				}]
			}
		},
		"AutoScalingGroupForEdgeServer": {
			"Type": "AWS::AutoScaling::AutoScalingGroup",
			"Metadata": {
				"Comment": "Auto scaling group for Edge server"
			},
			"Properties": {
				"DesiredCapacity": "2",
				"HealthCheckType": "EC2",
				"LaunchConfigurationName": {
					"Ref": "LaunchConfigForEdgeServer"
				},
				"MaxSize": "2",
				"MetricsCollection": [{
					"Granularity": "1Minute",
					"Metrics": [
						"GroupDesiredCapacity"
					]
				}],
				"MinSize": "2",
				"TargetGroupARNs": [{
						"Ref": "EdgeServerTargetGroup"
					}

				],
				"NotificationConfigurations": [{
						"TopicARN": {
							"Ref": "BOIScaleUpMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_LAUNCH",
							"autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
						]
					},
					{
						"TopicARN": {
							"Ref": "BOIScaleDownMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_TERMINATE",
							"autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
						]
					}
				],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["EdgeServer_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					},
					"PropagateAtLaunch": "true"
				}],
				"VPCZoneIdentifier": [{
						"Ref": "ZuulSubnetAZ1"
					},
					{
						"Ref": "ZuulSubnetAZ2"
					}
				]
			},
			"UpdatePolicy": {
				"AutoScalingScheduledAction": {
					"IgnoreUnmodifiedGroupSizeProperties": "true"
				},
				"AutoScalingRollingUpdate": {
					"MaxBatchSize": "1",
					"MinInstancesInService": "1",
					"PauseTime": "PT10M",
					"WaitOnResourceSignals": "false"
				}
			}
		},
		"SSHBastionToEdgeIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "22",
				"GroupId": {
					"Ref": "EdgeServerSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"ToPort": "22"
			}
		},
		"HTTPSLBToEdgeIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "443",
				"GroupId": {
					"Ref": "EdgeServerSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "LBSecurityGroup"
				},
				"ToPort": "443"
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ1ToZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ1 To Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "100",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"SSHAllowInboundNACLEntryFromBastionAZ2ToZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound SSH traffic From Bastion AZ2 To Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "110",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "BastionSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "22",
					"To": "22"
				}
			}
		},
		"EphimeralAllowInboundNACLEntryFromZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound traffic From Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "120",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "32768",
					"To": "65535"
				}
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ1ToZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "130",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "443",
					"To": "443"
				}
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ2ToZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "140",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "443",
					"To": "443"
				}
			}
		},
		"EphimeralAllowOutboundNACLEntryFromZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound Ephimeral traffic From Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "121",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "32768",
					"To": "65535"
				}
			}
		},
		"VPCAllAllowOutboundNACLEntryFromZuulSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Zuul Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "ZUULACL"
				},
				"RuleNumber": "151",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "true",
				"CidrBlock": {
					"Ref": "VPCCidrBlock"
				},
				"PortRange": {
					"From": "0",
					"To": "65535"
				}
			}
		},
		"AlarmForInstanceStatusCheckOfEdgeServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for EdgeServerCluster servers it goes active when the status check of any instance in EdgeServerClusterautoscaling group fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for EdgeServerCluster server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "AutoScalingGroupName",
					"Value": {
						"Ref": "AutoScalingGroupForEdgeServer"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1",
				"Unit": "Count"
			}
		},
		"CPUHighAlarmForEdgeServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for EdgeServerCluster servers it goes active when the CPU utilization is more than 75 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU alarm for my EdgeServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "EdgeServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"CPULowAlarmForEdgeServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for EdgeServerCluster servers it goes active when the CPU Utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU Low alarm for my EdgeServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "EdgeServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		},
		"MemoryHighAlarmForEdgeServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for EdgeServerCluster servers it goes active when the Memory utilization is more than 75 percent for 5 consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory High alarm for my EdgeServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "EdgeServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"MemoryLowAlarmForEdgeServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for EdgeServerCluster servers it goes active when the Memory utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [
					{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory Low alarm for my EdgeServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions":[{
					"Name": "ClusterName",
					"Value": "EdgeServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		}
	}
}