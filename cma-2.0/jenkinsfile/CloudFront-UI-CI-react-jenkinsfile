package com.javacodegeeks.groovy.date
import static java.util.Calendar.*

def checkString(value) {
    if ( value != null && !value.isEmpty() && !value.trim().isEmpty() && value != "null"){
        return true
    }else {
        return false
    }
}
def b365ScaBuildVersion = new Date().format("yyMMddHHmm")
pipeline {
    parameters {
        choice(name: 'gitProjectRepo', choices: 'git@bitbucket.org:psdapi/sandboxv3-sca-ui.git\ngit@bitbucket.org:cap-gemini/cap-gemini/portals-ui.git\ngit@bitbucket.org:cap-gemini/psd2-sca-ui.git\ngit@bitbucket.org:cap-gemini/psd2-boi-consent-ui.git\ngit@bitbucket.org:cap-gemini/psd2-boi-sca-ui.git\ngit@bitbucket.org:psdapi/consent-ui-cma.git\ngit@bitbucket.org:psdapi/psd2-sca-ui.git', description: 'Project repo')
        string(name: 'gitProjectBranch', defaultValue: 'master',description: 'Branch to build')
        string(name: 'artifactoryLocation', defaultValue: 'boimayrelireland',description: 'Project  repo')
        choice(name: 'unzipDirectory', choices: 'SaaSAppWebApplication/src/main/resources/static/assets/sca-ui', description: 'unzip build.zip to that folder' )
        string(name: 'gitCredentialsId', defaultValue: 'c1d2c6d6-22d0-40ce-9e11-933c49bfd995', description: 'Credentials')
        booleanParam(name: 'deploy', defaultValue:true ,description: "skip test for maven build")
        choice(name: "awsCredentailsId",choices: "AWS_Cred_boiTest2\nAMS_BOIDEV",description: "AWS Access key and Secret")
        choice(name: "cloudfrontReleaseName",choices: "v1\nv2\nv3\nv4",description: "AWS Access key and Secret")
        choice(name: 'cloudfrontS3BucketName', choices: 'psd2.apiboidev.com\ncloudfront-sandbox-boitest\nboi-api-platform-cloudfront-mayrel\nboi-boi-api-platform-cloudfront-mayreltest2-mayreltest2\nboi-api-platform-cloudfront-mayrelireland\nboi-api-platform-cloudfront-test\nsandbox-prepod-cloudfrong-temp', description: 'Project  repo')
        string(name: 'cloudfrontDistributionId', defaultValue: 'E3CJ14RFYUHX61', description: 'jobs success report send to this mail')
        choice(name: "cloudfrontS3BucketChildPath", choices: "sca-ui" )
        choice(name: "cloudfrontS3BucketChildDirectoryPath", choices: "static/js" )
        choice(name: 'gitProjectCodeRepo', choices: 'git@bitbucket.org:psdapi/sandboxv3-sca-ui.git\ngit@bitbucket.org:cg-psd2/test_to_delete.git\ngit@bitbucket.org:cg-psd2/cma2_card_release_v2.1.0.git', description: 'Project  repo')
        choice(name: "configYamlFile",choices: "saasv1-boiDev.yml\nsaasv1-boiTest.yml\nsaasv1-sandboxboiTest.yml\nsaasv1-sandboxboiTest.yml",description: "YAML Changes")
        choice(name: "gitYamlRepo",choices: "git@bitbucket.org:psdapi/sandboxv3-dev-config.git\ngit@bitbucket.org:psdapi/sandboxv3-dev-config.git\ngit@bitbucket.org:cg-psd2/test_to_delete_yml.git\ngit@bitbucket.org:cg-psd2/psd2-config-application-boiTest-mcr-0.7.1-IrelandCMA.git\ngit@bitbucket.org:cg-psd2/psd2-config-application-boitest-mcr-0.7.1_frankfurtcma.git\ngit@bitbucket.org:cg-psd2/psd2-config-application-frankfurt_ams_test.git\ngit@bitbucket.org:psdapi/psd2-config-application-sandbox-preprod.git",description: "YAML Changes for Git Repo")
    }
    agent { node { label 'master' } }
    stages {
        stage ('clean workspace') {
            steps {
                deleteDir()
            }
        }
        stage ('check deployment parameters value') {
            when {
                expression {
                    checkString(params.artifactoryLocation) != true
                }
            }
            steps {
                script {
                    error "please upload artifactory location where you want to push zip file"
                }
            }
        }
        stage ("git clone project"){
            steps {
                git branch: "${params.gitProjectBranch}", credentialsId: "${params.gitCredentialsId}", url: "${params.gitProjectRepo}"
            }
        }
        stage('install npm package') {
            steps {
                sh 'npm install'
            }
        }
		stage ('Replace buildverion in path.js'){
            steps {
                  sh "sed -i \"s;_DESTINATION_;$b365ScaBuildVersion;g\" config/paths.js"
            }
        }
        stage('check folder exist for code coverage'){
            steps {
                script {
                        println "pushing sonar report to sonar server"
                        sh "npm run build"
                }
            }
        }
        stage('check directory exist') {
            steps {
                script {
                    if ( !fileExists('build')) {
                        error "build directory is not exist"
                    } else {
                        echo "build directory exist"
                        zip archive: true, dir: 'build', zipFile: 'build.zip'
                    }
                }
            }
        }
        stage('upload the artifactory ') {
            steps {
                script {
                    def server = Artifactory.server('artifactory')
                    def buildInfo = Artifactory.newBuildInfo()
                    def uploadSpec = """{
                    "files": [
                    {
                    "pattern": "build.zip",
                    "target": "lib-release-ui/${params.cloudfrontReleaseName}/${params.artifactoryLocation}/${env.BUILD_ID}/"
                    }
                    ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }

    }
    post {
        success {
            script {
                if (params.deploy == true && checkString(params.awsCredentailsId) == true && checkString(params.artifactoryLocation) == true && checkString(params.cloudfrontS3BucketName) == true  && checkString(params.cloudfrontReleaseName) == true && checkString(params.cloudfrontDistributionId) == true && checkString(params.unzipDirectory) == true )  {
                    build job: 'CloudFront-UI-CD-react', parameters: [
                        [$class: 'StringParameterValue',  name: 'awsCredentailsId', value: "${params.awsCredentailsId}"],
                        [$class: 'StringParameterValue',  name: 'artifactoryLocation', value: "${params.artifactoryLocation}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontReleaseName', value: "${params.cloudfrontReleaseName}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontS3BucketName', value: "${params.cloudfrontS3BucketName}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontDistributionId', value: "${params.cloudfrontDistributionId}"],
                        [$class: 'StringParameterValue',  name: 'upStreamBuildNumber', value: "${env.BUILD_NUMBER}"],
                        [$class: 'StringParameterValue',  name: 'gitProjectRepo', value: "${params.gitProjectCodeRepo}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontS3BucketChildPath', value: "${params.cloudfrontS3BucketChildPath}"],
                        [$class: 'StringParameterValue',  name: 'gitYamlRepo', value: "${params.gitYamlRepo}"],
                        [$class: 'StringParameterValue',  name: 'configYamlFile', value: "${params.configYamlFile}"],
                        [$class: 'StringParameterValue',  name: 'unzipDirectory', value: "${params.unzipDirectory}"],
                        [$class: 'StringParameterValue',  name: 'b365ScaBuildVersion', value: "${b365ScaBuildVersion}"]]
                } else if (params.deploy == true && checkString(params.awsCredentailsId) == true && checkString(params.artifactoryLocation) == true && checkString(params.cloudfrontS3BucketName) == true  && checkString(params.cloudfrontReleaseName) == true && checkString(params.unzipDirectory) == true ) {
                    build job: 'CloudFront-UI-CD-react', parameters: [
                        [$class: 'StringParameterValue',  name: 'awsCredentailsId', value: "${params.awsCredentailsId}"],
                        [$class: 'StringParameterValue',  name: 'artifactoryLocation', value: "${params.artifactoryLocation}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontReleaseName', value: "${params.cloudfrontReleaseName}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontS3BucketName', value: "${params.cloudfrontS3BucketName}"],
                        [$class: 'StringParameterValue',  name: 'upStreamBuildNumber', value: "${env.BUILD_NUMBER}"],
                        [$class: 'StringParameterValue',  name: 'gitProjectRepo', value: "${params.gitProjectCodeRepo}"],
                        [$class: 'StringParameterValue',  name: 'cloudfrontS3BucketChildPath', value: "${params.cloudfrontS3BucketChildPath}"],
                        [$class: 'StringParameterValue',  name: 'gitYamlRepo', value: "${params.gitYamlRepo}"],
                        [$class: 'StringParameterValue',  name: 'configYamlFile', value: "${params.configYamlFile}"],
                        [$class: 'StringParameterValue',  name: 'unzipDirectory', value: "${params.unzipDirectory}"],
                        [$class: 'StringParameterValue',  name: 'b365ScaBuildVersion', value: "${b365ScaBuildVersion}"]]
                }
            }
        }
    }
}