import groovy.json.*
import hudson.FilePath;
import jenkins.model.Jenkins;
def checkString(value) {
    if ( value != null && !value.isEmpty() && !value.trim().isEmpty() && value != "null"){
        return true
    }else {
        return false
    }
}
def readFileCommon(path) {
    if (env['NODE_NAME'] == null ) {
        error "please define master server"
    } else if (env['NODE_NAME'].equals("master")) {
        return new FilePath(null, path).readToString();
    } else {
        return new FilePath(Jenkins.getInstance().getComputer(env['NODE_NAME']).getChannel(), path).readToString();
    }
}
def jenkinsScriptFolder
def dockerImageName
def dockerTagName
def profileMatchEntrypoint
def versionName
def parametersMaps = [:]
pipeline {
    agent { node { label 'jenkins-slave1' } }
    parameters {
    	choice(name: 'profile', choices:"domestic-payment-consents-fs\naccount-information-raml-fs\ncustomer-account-profile-mule-fs\naccount-statements-fs\naccount-balance\naccount-beneficiaries\naccount-directdebits\ndomestic-payment-consents\ndomestic-payments\ninternational-payment-consents\ninternational-payments\naccount-information\naccount-product\naccount-request\naccount-standingorder\naccount-transaction\nintegration-operations\npayment-setup\npayment-submission\npayment-submission-retrieve\ntpp-block\nconsent-revocation\nsaas-application\ntpp-portal\nconsent-application\neureka-server\nconfig-server\nedge-server\naccount-information-raml-fs\naccount-transactions-raml-fs\naccount-schedule-payments\naccount-statements\naccount-scheduled-payments-fs\nfunds-confirmation-consent\nfunds-confirmation\naccount-product-fs\ndomestic-scheduled-payments-fs\ndomestic-scheduled-payments\ndynamic-client", description: 'profile Name for build Maven Project')
        choice(name: 'others', choices: 'boiTest\nnative\ndefault', description: 'others')
        choice(name: 'version', choices: "v1\nv2\nv3\nNA", description: "Version which is to be deployed")
		string(name: 'GitBranch', defaultValue: 'feature/PreProd-Sprint18', description: 'Branch of gitProjectRepo to checkout')
		string(name: 'GitTag', defaultValue: '', description: 'Tag of gitProjectRepo to checkout')
    }
    environment {
        mavenProfileCheck=false
    }
      stages {
        stage ('clean workspace') {
          steps {
            deleteDir()
          }
        }
        stage('check profile value') {
          when {
            expression {
              return ! checkString(params.profile)
            }
          }
          steps {
            script {
              error "please provide profile name"
            }
          }
        }
        stage ('get parameters details for environment file') {
            agent { node { label 'master' } }
            steps {
                script {
                    def getParentPathJobs = new File("${env.WORKSPACE}").getParent()
                    def workspaceLocation = "${env.WORKSPACE}@script"
                    jenkinsScriptFolder = new File("${workspaceLocation}")
                    println jenkinsScriptFolder
                    if ( ! jenkinsScriptFolder.exists()){
                      error "jenkins script directory does not exist"
                    }
                    def environmentFileLocation = "${jenkinsScriptFolder}" + File.separator + "common" + File.separator + "deployment" + File.separator + "environmentParameters.json"
                    def jsondata = readFileCommon("${environmentFileLocation}")
                    def parsedJson = new groovy.json.JsonSlurper().parseText(jsondata)
                    if (parsedJson.get('environment')) {
                        parsedJson.get('environment').get(params.others).each { key, value ->
                        println key
                         if (value instanceof Map) {
                            value.each { mapKey, mapValue ->
                              if (!(mapValue instanceof Map)) {
                                parametersMaps[mapKey] = mapValue
                              }else if (mapValue instanceof Map) {
                                if ( mapKey == "${params.profile}" && mapValue instanceof Map){
                                    parametersMaps['profile'] = mapKey
                                    mapValue.each { profilekey, profilevalue ->
                                    if ( profilevalue instanceof Map) {
                                      value.each { k, v ->
                                        parametersMaps[k] = v
                                      }
                                    } else if (!(profilevalue instanceof Map))  {
                                      parametersMaps[profilekey] = profilevalue
                                    }
                                  }
                                } else if (mapKey == "${params.version}" && mapValue instanceof Map ) {
                                  parametersMaps['version'] = mapKey
                                  mapValue.each { versionKey, versionValue ->
                                    parametersMaps[versionKey] = versionValue
                                  }
                                }
                              }
                            }
                         } else {
                             parametersMaps[key]=value
                         }
                      }
                    } else {
                      error "unable to parse environment value"
                    }
                    if (parsedJson.get("profiles")) {
                      parsedJson.get("profiles").each { key, value ->
                      if ( key == "${params.profile}" && value instanceof Map) {
                        parametersMaps['profile'] = "${params.profile}"
                        value.each { k, v ->
                            if ( v instanceof Map){
                              v.each { profilekey, profilevalue ->
                                parametersMaps[profilekey] = profilevalue
                              }
                            } else {
                              parametersMaps[k] = v
                            }
                        }
                      }
                    }
                  }
                }
            }
        }
        stage ('create docker registry url') {
          steps {
            script {
              if (checkString(parametersMaps.aws_credential)){
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: "${parametersMaps.aws_credential}", secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                  def aws_account_number = sh(script: "aws sts get-caller-identity --no-verify-ssl | jq -r '.Account'", returnStdout: true)
                  if (checkString(aws_account_number) && checkString(parametersMaps.region)){
                    parametersMaps['URL'] = aws_account_number.trim() + ".dkr.ecr.${parametersMaps.region}.amazonaws.com"
                  } else {
                    error "unable to get account number"
                  }
                }
              }
            }
          }
        }
        stage('get docker tag Name') {
          agent { node { label 'master' } }
          steps {
            script {
              if ( ! jenkinsScriptFolder.exists()){
                error "jenkins script directory does not exist"
              }
              def dockerTagFileLocation = "${jenkinsScriptFolder}" + File.separator + "common" + File.separator + "deployment" + File.separator + "sourceTagName.json"
              println dockerTagFileLocation
              def imageTagData = readFileCommon("${dockerTagFileLocation}")

              def imageTagJson = new groovy.json.JsonSlurper().parseText(imageTagData)
              println imageTagJson
              println parametersMaps
              if (imageTagJson.get("tags").get(parametersMaps.version)){
                imageTagJson.get("tags").get(parametersMaps.version).get(parametersMaps.releaseName).each { sourceTags ->
                  switch(sourceTags.active) {
                    case "true":
                      dockerTagName = sourceTags.tagName
                      break
                  }
                }
              } else {
                error "there is no tag create for this release"
              }
            }
          }
        }
        stage ("git clone project"){
            steps {
                script {
                    git branch: "${params.GitBranch}", credentialsId: "${parametersMaps.gitCredentialsId}", url: "${parametersMaps.gitProjectRepo}"
					if (checkString(params.GitTag)){
                        sh "git checkout ${params.GitTag}"
                     }

                }
            }
        }
        stage ('check profile name against entrypoint name'){
            steps {
                script {
                    println parametersMaps
                    if (checkString(parametersMaps.entrypointCheck)){
                      profileMatchEntrypoint = "${parametersMaps.entrypointCheck}"
                    } else {
                      if (checkString(parametersMaps.profile) && "${parametersMaps.profile}".contains('-fs')){
                          profileMatchEntrypoint = "${parametersMaps.profile}".trim().split('-').collect{ it.capitalize() }.join('')
                          profileMatchEntrypoint = profileMatchEntrypoint.substring(0, profileMatchEntrypoint.length() - 2) + "MockFoundationService"
                      }else {
                          profileMatchEntrypoint = "${parametersMaps.profile}".trim().split('-').collect{ it.capitalize() }.join('')
                      }
                    }
                    if ("${parametersMaps.entryPointPath}".trim().split('/')[0]==profileMatchEntrypoint){
                        mavenProfileCheck=true
                    } else{
                        error "please select right entrypoint for ${parametersMaps.profile}"
                    }
                }
            }
        }
        stage ('get clone devops config repo') {
          steps {
            script {
              if (checkString(parametersMaps.gitDevposConfigRepo) && checkString(parametersMaps.gitCredentialsId) && checkString(parametersMaps.gitDevopsBranch)) {
                sh "mkdir devops"
                dir('devops'){
                    git branch: "${parametersMaps.gitDevopsBranch}", credentialsId: "${parametersMaps.gitCredentialsId}", url: "${parametersMaps.gitDevposConfigRepo}"
                }
              } else {
                error "unable to clone devops config repo"
              }
            }
          }
        }
        stage('check required parameters for this build') {
          when {
            expression {
              return (!checkString(parametersMaps.releaseName) && !checkString(parametersMaps.URL) && !checkString(parametersMaps.region) && !checkString(parametersMaps.clusterName) && !checkString(parametersMaps.keyStoreName))
            }
          }
          steps {
            error "please provide releaseName, URL, Region and clusterName, keyStoreName"
          }
        }
        stage ('maven build'){
            steps {
                script {
                    def server = Artifactory.server('artifactory')
                    def buildInfo = Artifactory.newBuildInfo()
                    // maven build - plugin auto deploys the build artifact to the specifid repo
                    def rtMaven = Artifactory.newMavenBuild()
                    rtMaven.tool = 'm2'
                    if (checkString(parametersMaps.classpathBuildAggregate)){
                      dir("${parametersMaps.classpathBuildAggregate}") {
                          rtMaven.run pom: 'pom.xml', goals: 'clean install ' + ' -DskipTests=' +"${parametersMaps.skipTest}" + ' -P '+"${parametersMaps.profile}" + ' sonar:sonar -Dsonar.branch='+"${parametersMaps.profile}", buildInfo: buildInfo
                      }
                      sh "mkdir ${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/classpath"
                      if ( !checkString(parametersMaps.otherBuildAggregate)){
                        sh "find . -type f  -name *.jar ! -path \"*/.mvn/*\" ! -path \"*@tmp*\"  | xargs -I{} cp -r {} ${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/classpath"
                      }
                    }
                    if (checkString(parametersMaps.otherBuildAggregate)){
                      def otherBuildAggregateList = "${parametersMaps.otherBuildAggregate}".split(",")
                      for (dependecies in otherBuildAggregateList) {
                        def dependeciesBuildAggregateAndProfile = "${dependecies}".split(":")
                        dir("${dependeciesBuildAggregateAndProfile[1]}") {
                            rtMaven.run pom: 'pom.xml', goals: 'clean install ' + ' -DskipTests=' +"${parametersMaps.skipTest}" + ' -P '+"${dependeciesBuildAggregateAndProfile[0]}" + ' sonar:sonar -Dsonar.branch='+"${dependeciesBuildAggregateAndProfile[0]}", buildInfo: buildInfo
                        }
                      }
                      if (! fileExists("${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/classpath")){
                        sh "mkdir ${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/classpath"
                      }
                      sh "find . -type f  -name *.jar ! -path \"*/.mvn/*\" ! -path \"*@tmp*\"  | xargs -I{} cp -r {} ${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/classpath"
                    }
                    if (mavenProfileCheck==true && checkString(parametersMaps.mainBuildAggregate)){
                        dir ("${parametersMaps.mainBuildAggregate}"){
                            rtMaven.run pom: 'pom.xml', goals: 'clean install ' + ' -DskipTests=' +"${parametersMaps.skipTest}" +' -P '+"${params.profile}" + ' sonar:sonar -Dsonar.branch='+"${parametersMaps.profile}", buildInfo: buildInfo
                        }
                        sh "cp ${parametersMaps.entryPointPath}/target/*.jar ${env.WORKSPACE}/devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}/"
                        server.publishBuildInfo buildInfo
                    } else {
                      error "mainBuildAggregate is missing from environment file"
                    }
                }
            }
        }
        stage ('docker build and push the images') {
            steps {
                dir("devops/${parametersMaps.releaseName}/Docker/${parametersMaps.profile}"){
                    script {
                            versionName = ("${params.version}")
                            println versionName as String
                            if ( versionName == "NA"){
                                dockerImageName = ("${parametersMaps.profile}".toLowerCase())
                                print "Into IF loop for dockerImageName"
                            }
                            else {
                                dockerImageName = ("${parametersMaps.profile}".toLowerCase()+"-"+"${params.version}")
                                print "Into ELSE loop for dockerImageName"
                            }
                        withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: "${parametersMaps.aws_credential}", secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                            
							println dockerImageName as String
                            def awsLogin  = sh(script: "aws ecr get-login --region ${parametersMaps.region}", returnStdout: true)
                            sh "${awsLogin}"
                            if (checkString(dockerTagName)){
                              docker.build("${parametersMaps.URL}/${dockerImageName}:${dockerTagName}${BUILD_NUMBER}","-f ${parametersMaps.profile}-Dockerfile .").push()
                            } else {
                                error "unable to create docker images and push"
                            }

                        }
                    }
                }
            }
        }
      }
      post {
          always {
              archive "${parametersMaps.entryPointPath}/target/*.jar"
              //deleteDir() /* clean up our workspace */
          }
          success {
              script {
                  sh "docker rmi ${parametersMaps.URL}/${dockerImageName}:${dockerTagName}${BUILD_NUMBER}"
                  if (checkString(parametersMaps.codeTagging)) {
                      build job: 'CodeTagging', parameters: [
                          [$class: 'StringParameterValue',  name: 'codeTagging', value: "${parametersMaps.codeTagging}"],
                          [$class: 'StringParameterValue',  name: 'profile', value: "${parametersMaps.profile}"],
                          [$class: 'StringParameterValue',  name: 'URL', value: "${parametersMaps.URL}"],
                          [$class: 'StringParameterValue',  name: 'upstreamBuildNumber', value: "${env.BUILD_NUMBER}"],
                          [$class: 'StringParameterValue',  name: 'gitProjectRepo', value: "${parametersMaps.gitProjectRepo}"]]
                  }
                  if (checkString(params.others) && checkString(parametersMaps.profile) && checkString(parametersMaps.version)) {
                      build job: 'Common-CD', parameters: [
                          [$class: 'StringParameterValue',  name: 'others', value: "${params.others}"],
                          [$class: 'StringParameterValue',  name: 'dockerBuildNumber', value: "${env.BUILD_NUMBER}"],
                          [$class: 'StringParameterValue',  name: 'profile', value: "${parametersMaps.profile}"],
                          [$class: 'StringParameterValue',  name: 'version', value: "${parametersMaps.version}"]
                        ]
                      }
                  }
              }
          }
      }