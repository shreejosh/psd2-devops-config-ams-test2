#!/bin/bash

#-------------------
# EC2 Instance Variables
#-------------------
AWS_INSTANCEIP="`curl -k -s http://169.254.169.254/latest/meta-data/local-ipv4`"
AWS_INSTANCEID="`curl -k -s http://169.254.169.254/latest/meta-data/instance-id`"
AWS_REGION="`curl -k -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/[abcd]$//g'`"
AWS_USERDATA="`curl -k -s http://169.254.169.254/latest/user-data > /tmp/aws-user-data`"
AWS_AZ="`curl -K -s http://169.254.169.254/latest/meta-data/placement/availability-zone`"

function ExportParameters() {
  checkStringExists=`grep "export ${1}=" ${3} | wc -l`
  if (( ${checkStringExists} > 0 ));then
    SedValue REPLACE ";" "^export ${1}=.*" "export ${1}=${2}" ${3}
  else
    echo "export ${1}=${2}" >> ${3}
  fi
}

function ReplaceOrAppendOnFile() {
  if [[ -f ${3} ]]; then
    checkStringExists=`grep "${1}" ${3} | wc -l`
  fi
  if (( ${checkStringExists} > 0 ));then
    SedValue REPLACE ";" "${1}.*" "${1}${2}" ${3}
  else
    if [[ -f ${3} ]]; then
      echo "${1}${2}" >> ${3}
    else
      echo "${1}${2}" > ${3}
    fi
  fi
}

function SedValue() {
  METHOD=${1}
  DELIMITER=${2}
  case ${METHOD} in
    REPLACE)
      sed -i "s${DELIMITER}${3}${DELIMITER}${4}${DELIMITER}g" ${5}
      ;;
    BEFORE)
      sed -i "${DELIMITER}${3}${DELIMITER}${4}" ${5}
     ;;
    ADD)
     sed -i "${DELIMITER}${3}${DELIMITER}" ${4}
     ;;
  esac
}

function ProxySettingsforSSM() {
  declare -A Parameters_Array
  ProxyServer=http://${1}
  NoProxyServer=169.254.169.254,.webservices.com
  SsmFileName="/etc/init/amazon-ssm-agent.conf"
  Parameters_Array=([http_proxy]="env" [HTTP_PROXY]="env" [https_proxy]="env" [HTTPS_PROXY]="env" [no_proxy]="env")
  for Parameters in ${!Parameters_Array[@]}; do
    checkStringExists=`grep "${Parameters_Array[$Parameters]} ${Parameters}=" ${SsmFileName} | wc -l`
    echo ${checkStringExists}
    if (( ${checkStringExists} > 0 ));then
      if [[ "${Parameters}" == "no_proxy" ]]; then
        SedValue REPLACE ";" "^${Parameters_Array[$Parameters]} ${Parameters}=.*" "${Parameters_Array[$Parameters]} ${Parameters}=${NoProxyServer}" ${SsmFileName}
      else
        SedValue REPLACE ";" "^${Parameters_Array[$Parameters]} ${Parameters}=.*" "${Parameters_Array[$Parameters]} ${Parameters}=${ProxyServer}" ${SsmFileName}
      fi
    else
      if [[ "${Parameters}" == "no_proxy" ]]; then
        SedValue BEFORE "/" "exec" "i\\${Parameters_Array[$Parameters]} ${Parameters}=${NoProxyServer}" ${SsmFileName}
      else
        SedValue BEFORE "/" "exec" "i\\${Parameters_Array[$Parameters]} ${Parameters}=${ProxyServer}" ${SsmFileName}
      fi
    fi
  done
}

function ProxySettings() {
  HttpProxyIporDNS=${1}
  declare -A Parameters_Array
  Parameters_Array=([http_proxy]="http://${HttpProxyIporDNS}" [https_proxy]="http://${HttpProxyIporDNS}" [NO_PROXY]="169.254.169.254,.webservices.com,.mockfoundationservices.com")
  File_Names=${2}
  for Parameters in ${!Parameters_Array[@]}; do
    for filename in ${File_Names[@]}; do
      if [[ -f ${filename} ]]; then
        ExportParameters ${Parameters} ${Parameters_Array[$Parameters]} ${filename}
      else
        echo "${filename} does not exist"
      fi
    done
    source "${filename}"
  done
}

function ProxySettingsForEcsAgent() {
  HttpProxyIporDNS=${1}
  declare -A Parameters_Array
  Parameters_Array=([http_proxy]="${HttpProxyIporDNS}" [https_proxy]="${HttpProxyIporDNS}" [NO_PROXY]="169.254.169.254,169.254.170.2,/var/run/docker.sock")
  ecsAgentFileName="/etc/ecs/ecs.config"
  for Parameters in ${!Parameters_Array[@]}; do
      ReplaceOrAppendOnFile "${Parameters}=" "${Parameters_Array[$Parameters]}" ${ecsAgentFileName}
  done
}

function ProxySettingsForYum() {
  HttpProxyIporDNS=${1}
  declare -A Parameters_Array
  Parameters_Array=([proxy]="${HttpProxyIporDNS}")
  ecsAgentFileName="/etc/yum.config"
  for Parameters in ${!Parameters_Array[@]}; do
      ReplaceOrAppendOnFile "${Parameters}=" "${Parameters_Array[$Parameters]}" ${ecsAgentFileName}
  done
}

function CreateDnsARecord() {
  HostedZoneId=${1}
  changeJsonFile=${2}
  dnsName=${3}
  IpAddress=${4}
  if [[ -f ${changeJsonFile} ]]; then
    ReplaceOrAppendOnFile "\"Name\": " "\"${dnsName}\"," ${changeJsonFile}
    ReplaceOrAppendOnFile "\"Value\": " "\"${IpAddress}\"" ${changeJsonFile}
    aws route53 change-resource-record-sets --hosted-zone-id ${HostedZoneId}  --change-batch file://${changeJsonFile}
  else
    echo "${changeJsonFile} does not exist and unable to create the dns record"
  fi
}

function SmtpPassGenerate() {
  IAMSECRET=${1}
  if [[ -n ${IAMSECRET} ]]; then
    MSG="SendRawEmail";
    VerInBytes="2";
    VerInBytes=$(printf \\$(printf '%03o' "$VerInBytes"));
    SignInBytes=$(echo -n "$MSG"|openssl dgst -sha256 -hmac "$IAMSECRET" -binary);
    SignAndVer=""$VerInBytes""$SignInBytes"";
    SmtpPass=$(echo -n "$SignAndVer"|base64);
  else
    echo "Password ${IAMSECRET} doesn't exist"
  fi
}

function InstallPackage() {
  if [[ -n ${1} ]]; then
    yum install -y ${1}
  elif [[ -n ${2} ]]; then
    StartOrStopService ${2}
  fi
}

function StartOrStopService() {
  stop ${1}
  start ${1}
}

function restartDocker() {
  ecsAgent=${1}
  service docker restart
  if [[ -n ${ecsAgent} ]]; then
    StartOrStopService ${ecsAgent}
  fi
}

function ConfigureNTP() {
  ntpDnsName=${1}
  ntpFileName=${2}
  if [[ -n ${ntpDnsName} && -n ${ntpFileName} ]];then
    SedValue REPLACE ";" "^server.*" "server ${ntpDnsName} iburst" ${ntpFileName}
    service ntpd restart
  fi
}

function ConfigureEFS() {
  DIR_TGT=${1}
  EFS_FILE_SYSTEM_ID=${2}
  if [[ ! -d ${DIR_TGT} ]]; then
      mkdir -p ${DIR_TGT}
  fi
  if [[ -z ${EFS_FILE_SYSTEM_ID} && -z ${AWS_AZ} && -z ${AWS_REGION} ]]; then
    echo "please provide efs filesystem id and region and az details"
    exit 1
  fi
  DIR_SRC="$AWS_AZ.$EFS_FILE_SYSTEM_ID.efs.$AWS_REGION.amazonaws.com"
  if [[ -n ${DIR_TGT} && -n ${DIR_SRC} ]]; then
    echo "create efs entry on /etc/fstab"
    ReplaceOrAppendOnFile "$DIR_SRC:/"  "  $DIR_TGT  nfs  defaults  0  0" /etc/fstab
    echo "mounting efs"
    mount -a
  else
    echo "unable to mount the efs please your proxy setting and outbound port"
  fi
}

function DeteleKeyAndAddKeyIntoFile() {
  Key=${1}
  checkStringExists=`grep "^${Key}" ${3} | wc -l`
  echo "${checkStringExists}"
  regex="-IG[[:digit:]]"
  if [[ ${Key} =~ $regex ]]; then
    Key=${Key//$regex/}
  fi
  if (( ${checkStringExists} > 0 ));then
    sed -i "/^${Key}.*/d" ${3}
  fi
  if [[ ! -f ${3}  ]]; then
    touch ${3}
  fi
  echo "${Key}${2}" >> ${3}
}

function getTagsByInstanceId() {
  TagName=`aws ec2 describe-tags --filters Name=resource-id,Values=${AWS_INSTANCEID} Name=key,Values=Name --query Tags[].Value --output text --region ${AWS_REGION}`
}

function ConfigureAwsLogAgent() {
  proxy_server_aws_log=${1}
  if [[ -z ${proxy_server_aws_log} ]]; then
    echo "please provide proxy server ip address"
    exit 1
  fi
  if [[ ! -d /var/log/conf_file ]]; then
     mkdir -p /var/log/conf_file
  fi
  declare -A alias_export_dicts
  filename="/etc/bashrc"
  rsyslog_file="/etc/rsyslog.d/bash.conf"
  awslogs_python_script="/var/log/conf_file/awslogs-agent-setup.py"
  awslogs_proxyconfig_file="/var/awslogs/etc/proxy.conf"
  alias_export_dicts=([export-PROMPT_COMMAND]='$(logger -p local6.notice "User $USER has logged in")' [session_log_command()]=' { local status=$?;local command;command=$(history -a >(tee -a $HISTFILE));if [[ -n "$command" ]]; then logger -p local6.notice "Logged-in usr $USER [$$]: Executing command: $command"; history -c; history -r;fi }' [export-PROMPT_COMMAND-IG1]='session_log_command'
  [alias-exit]="'export PROMPT_COMMAND=\$(logger -p local6.notice \"User $USER has logged out\") && exit'" [alias-logout]="'export PROMPT_COMMAND=\$(logger -p local6.notice \"User $USER has logged out\") && exit'"  )
  for alias_export_vars in ${!alias_export_dicts[@]}; do
    IFS='-' read keyTag keyValue <<< ${alias_export_vars}
    if [[ -n  "${keyTag}" && -n "${keyValue}" ]]; then
        DeteleKeyAndAddKeyIntoFile "${keyTag} ${keyValue}=" "${alias_export_dicts[$alias_export_vars]}" "${filename}"
    else
      DeteleKeyAndAddKeyIntoFile "${alias_export_vars}" "${alias_export_dicts[$alias_export_vars]}" "${filename}"
    fi
  done
  source "/etc/bashrc"
  if [[ ! -f ${rsyslog_file} ]]; then
    touch ${rsyslog_file}
  fi
  DeteleKeyAndAddKeyIntoFile "local6.*" " /var/log/commands.log" "${rsyslog_file}"
  DeteleKeyAndAddKeyIntoFile "&" " ~" "${rsyslog_file}"
  service rsyslog restart
  if [[ ! -f /var/log/commands.log ]]; then
  touch /var/log/commands.log
  fi
  sed -i '/commands.log/d' /etc/logrotate.d/syslog
  sed -i '1 i/var/log/commands.log' /etc/logrotate.d/syslog
  if [[ ! -d /home/ec2-user/aws_scripts ]]; then
   mkdir -p /home/ec2-user/aws_scripts
  fi
  cp /home/ec2-user/aws_scripts/awscli-bundle.zip /root/
  if [[ -f /root/awscli-bundle.zip ]]; then
      cd /root/
      unzip -o awscli-bundle.zip
      whereis pip
      ln -s /usr/local/aws/bin/pip /usr/bin/pip
      ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
  else
     echo "file does not exist /root/awscli-bundle.zip"
     exit 1
  fi
  cp /home/ec2-user/aws_scripts/awslogs-agent-setup.py /var/log/conf_file/
  getTagsByInstanceId
  if [[ -n ${TagName} ]]; then
  echo -e "[general]
state_file = /var/awslogs/state/agent-state
[/var/log/messages]
file = /var/log/commands.log
log_group_name = /var/log/${TagName}
log_stream_name = $(hostname)
datetime_format = %b %d %H:%M:%S" > /var/log/conf_file/configuration_${AWS_INSTANCEID}
  else
    echo "TagName does not exist"
    exit 1
  fi
  sleep 5
    if [[ ! -d /var/awslogs/etc ]]; then
      mkdir -p  /var/awslogs/etc
    fi
  if [[ -f ${awslogs_python_script} ]]; then
    chmod a+x ${awslogs_python_script}
  else
    echo "aws log python script does not exist on ${awslogs_python_script} location"
    exit 1
  fi
  if [ -s /etc/issue ]; then
    sed -i "1s/.*/Amazon Linux AMI release 2017.03/" /etc/issue
  else
    sed -i '/^Amazon/d' /etc/issue
    echo -e 'Amazon Linux AMI release 2017.03' >> /etc/issue
  fi
  service awslogs restart
  echo "Restarted service awslogs"
  cd /var/log/conf_file
  chmod +x ./awslogs-agent-setup.py
  cp -f /home/ec2-user/aws_scripts/awslogs.tar /var/
  cp -f /home/ec2-user/aws_scripts/aws /var/awslogs/bin/
  cp -f /home/ec2-user/aws_scripts/activate /var/awslogs/bin/
  chmod +x /var/awslogs/bin/aws
  cd /var
  cp -f /home/ec2-user/aws_scripts/awslogs.tar /var/
  /bin/tar -xvf awslogs.tar
  if [[ ! -f /var/log/agentlog.txt ]]; then
    touch /var/log/agentlog.txt
  fi
  /usr/bin/python ${awslogs_python_script} --http-proxy http://${proxy_server_aws_log} --https-proxy http://${proxy_server_aws_log} --no-proxy 169.254.169.254  -n -r $AWS_REGION -c /var/log/conf_file/configuration_${AWS_INSTANCEID} | tee /var/log/agentlog.txt 2>&1
  sleep 2
  service awslogs restart
}
