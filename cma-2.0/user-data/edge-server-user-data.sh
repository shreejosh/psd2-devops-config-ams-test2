"#!/bin/bash\n",
{
  "Fn::Join": [
    "", [
      "HostedZoneId=",
      {
        "Ref": "PrivateHostedZoneId"
      },
      "\n"
    ],
    [
     "S3_BUCKET_NAME=",
     {
       "Ref":  "CloudFormationBucketName"
     },
     "\n"
    ],
    [
    "HttpProxyIporDNS=",
    {
      "Ref": "HttpProxyIporDNS"
    },
    "\n"
    ],
    [
    "DomainName=",
    {
      "Ref": "DomainName"
    },
    "\n"
    ],
    [
    "REGION=",
    {
      "Ref": "AWS::REGION"
    },
    "\n"
    ]
  ]
},
if [[ -n ${S3_BUCKET_NAME} && -n ${HttpProxyIporDNS} &&  -n ${DomainName} && -n ${REGION} ]]; then
  aws s3 --no-verify-ssl cp s3://${S3_BUCKET_NAME}/user-data/common.sh /root/common.sh --region ${REGION}
else
  echo "please provide Variables Value for S3_BUCKET_NAME, HttpProxyIporDNS,DomainName,REGION"
fi
if [[ -f "/root/common.sh" ]]; then
  source "/root/common.sh"
else
  echo "common libs does not exist"
  exit 1
fi
ECS_CLUSTER_NAME="EdgeServerCluster"
if [[ $?==0 ]]; then
  ProxySettingsforSSM ${HttpProxyIporDNS}
  ProxySettings ${HttpProxyIporDNS} "/etc/bashrc /etc/sysconfig/docker"
  if [[ -n ${ECS_CLUSTER_NAME} ]]; then
      ReplaceOrAppendOnFile "ECS_CLUSTER=" "${ECS_CLUSTER_NAME}" "/etc/ecs/ecs.config"
      ProxySettingsForEcsAgent ${HttpProxyIporDNS}
    fi
  if [[ -n ${HostedZoneId} && -n ${AWS_INSTANCEIP} && -n ${DomainName} ]]; then
    CreateDnsARecord ${HostedZoneId} "/root/change.json" "edgeserver.${DomainName}" "${AWS_INSTANCEIP}"
  fi
fi
