#! /var/lib/python
import sys, getopt
import requests
import jenkinsapi
import time
from jenkinsapi.jenkins import Jenkins

def disable_warnings(func):
    requests.packages.urllib3.disable_warnings()
    func()

def get_params():
    params={'profile':'account-balance', 'others':'boiTest','version':'v1'}
    return params

@disable_warnings
def main():
    jenkins=Jenkins('https://jenkins-test2.apiboitest.com', 'admin2', 'admin2', ssl_verify=False)
    job = jenkins['psd2-test2/Frankfurt/CMA2_Test_ENV/CMA2.0-Frankfurt/Common-Dev']
    qi = job.invoke(build_params=get_params())
    print(dir(qi))
    # Block this script until build is finished
    #if qi.is_queued() or qi.is_running():
    time.sleep(5)
    qi.block_until_complete()

    build = qi.get_build()
    print(build)


if __name__ == "__main__":
    main 
